﻿namespace CK.Ruben.Application.Tests.Commands.Handlers
{
    using AutoFixture;
    using CK.Ruben.Application.Commands.Handlers;
    using CK.Ruben.Application.Commands.Parameters;
    using CK.Ruben.Application.Exceptions;
    using CK.Ruben.Core.Entities;
    using CK.Ruben.Core.Repositories;
    using FluentAssertions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using System;
    using System.Threading.Tasks;

    [TestClass]
    public class RefundAuthorizeCommandHandlerTests
    {
        private RefundAuthorizeCommandHandler _sut;
        private Mock<IPaymentRepository> _paymentRepositoryMock;
        private Fixture _fixture;

        [TestInitialize]
        public void TestInitialize()
        {
            _paymentRepositoryMock = new Mock<IPaymentRepository>(MockBehavior.Strict);
            _sut = new RefundAuthorizeCommandHandler(
                paymentRepository: _paymentRepositoryMock.Object);

            _fixture = new Fixture();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_WithNullMediator_ShouldThrownArgumentNullException()
        {
            new RefundAuthorizeCommandHandler(
                paymentRepository: null);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidTransactionException))]
        public async Task Handle_WithNonExistingPayment_ShouldThrownException()
        {
            // Arrange
            var command = _fixture.Create<RefundAuthorizeCommand>();

            _paymentRepositoryMock
                .Setup(exe => exe.GetByIdAsync(
                    It.IsAny<Guid>()))
                .ReturnsAsync((Payments) null);

            // Act
            await _sut.Handle(command);
        }

        [TestMethod]
        [ExpectedException(typeof(VoidedTransactionException))]
        public async Task Handle_WithVoidedPayment_ShouldThrownException()
        {
            // Arrange
            var currentPayment = _fixture
                .Build<Payments>()
                .With(x => x.TransactionStatus, Core.Enums.TransactionStatus.Voided)
                .Create();

            var command = _fixture
                .Build<RefundAuthorizeCommand>()
                .With(x => x.PaymentId, currentPayment.PaymentId)
                .Create();

            _paymentRepositoryMock
                .Setup(exe => exe.GetByIdAsync(
                    It.IsAny<Guid>()))
                .ReturnsAsync(currentPayment);

            // Act
            await _sut.Handle(command);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidRefundAmountException))]
        public async Task Handle_WithInvalidCaptureAmount_ShouldThrownException()
        {
            // Arrange
            var currentPayment = _fixture
                .Build<Payments>()
                .With(x => x.TransactionStatus, Core.Enums.TransactionStatus.Created)
                .With(x => x.Amount, 10)
                .With(x => x.CapturedAmount, 0)
                .Create();

            var command = _fixture
                .Build<RefundAuthorizeCommand>()
                .With(x => x.PaymentId, currentPayment.PaymentId)
                .With(x => x.Amount, 11)
                .Create();

            _paymentRepositoryMock
                .Setup(exe => exe.GetByIdAsync(
                    It.IsAny<Guid>()))
                .ReturnsAsync(currentPayment);

            // Act
            await _sut.Handle(command);
        }

        [TestMethod]
        public async Task Handle_WithValidParameters_ShouldReturnMappedObject()
        {
            // Arrange
            var currentPayment = _fixture
                .Build<Payments>()
                .With(x => x.TransactionStatus, Core.Enums.TransactionStatus.Created)
                .With(x => x.Amount, 10)
                .With(x => x.CapturedAmount, 9)
                .With(x => x.UpdatedAt, DateTime.UtcNow.AddDays(1))
                .Create();

            Payments exepectedPayment = null;

            var command = _fixture
                .Build<RefundAuthorizeCommand>()
                .With(x => x.PaymentId, currentPayment.PaymentId)
                .With(x => x.Amount, 9)
                .Create();

            _paymentRepositoryMock
                .Setup(exe => exe.GetByIdAsync(
                    It.IsAny<Guid>()))
                .ReturnsAsync(currentPayment);

            _paymentRepositoryMock
                .Setup(exe => exe.UpdateAsync(
                    It.IsAny<Payments>()))
                .Callback<Payments>(pm => exepectedPayment = pm)
                .ReturnsAsync(currentPayment);

            // Act
            var result = await _sut.Handle(command);

            // Assert
            _paymentRepositoryMock
                .Verify(
                    expression: exe => exe.GetByIdAsync(
                        It.IsAny<Guid>()),
                    times: Times.Once);

            _paymentRepositoryMock
                .Verify(
                    expression: exe => exe.UpdateAsync(
                        It.IsAny<Payments>()),
                    times: Times.Once);

            result
                .Should()
                .NotBeNull();

            result
                .Amount
                .Should()
                .Be(10);

            exepectedPayment
                .UpdatedAt
                .Should()
                .BeCloseTo(DateTime.UtcNow, TimeSpan.FromSeconds(10));
        }
    }
}
