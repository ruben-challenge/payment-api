﻿namespace CK.Ruben.Application.Tests.Commands.Handlers
{
    using AutoFixture;
    using CK.Ruben.Application.Commands.Handlers;
    using CK.Ruben.Application.Commands.Parameters;
    using CK.Ruben.Application.Exceptions;
    using CK.Ruben.Application.Mappers;
    using CK.Ruben.Core.Entities;
    using CK.Ruben.Core.Repositories;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using System;
    using System.Threading.Tasks;

    [TestClass]
    public class CreateAuthorizeCommandHandlerTests
    {
        private CreateAuthorizeCommandHandler _sut;
        private Mock<IPaymentRepository> _paymentRepositoryMock;
        private Fixture _fixture;

        [TestInitialize]
        public void TestInitialize()
        {
            _paymentRepositoryMock = new Mock<IPaymentRepository>(MockBehavior.Strict);
            _sut = new CreateAuthorizeCommandHandler(
                paymentRepository: _paymentRepositoryMock.Object);

            _fixture = new Fixture();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_WithNullMediator_ShouldThrownArgumentNullException()
        {
            new CreateAuthorizeCommandHandler(
                paymentRepository: null);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task Handle_WithNullParameters_ShouldThrownException()
        {
            // Arrange
            CreateAuthorizeCommand command = null;

            // Act
            await _sut.Handle(command);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCreditCardNumbertException))]
        public async Task Handle_WithInvalidCardNumber_ShouldThrownException()
        {
            // Arrange
            var command = _fixture
                .Build<CreateAuthorizeCommand>()
                .With(x => x.CardNumber, "1234")
                .Create();

            // Act
            await _sut.Handle(command);
        }

        [TestMethod]
        public async Task Handle_WithValidParameters_ShouldReturnMappedObject()
        {
            // Arrange
            var command = _fixture
                .Build<CreateAuthorizeCommand>()
                .With(x => x.CardNumber, "4916938937029847")
                .With(x => x.CardData, "Daffie Davina")
                .With(x => x.ExpiryDate, "6/27")
                .With(x => x.Cvv, 656)
                .Create();

            var exepectedPayment = command.ToEntity();

            _paymentRepositoryMock
                .Setup(exe => exe.AddAsync(
                    It.IsAny<Payments>()))
                .ReturnsAsync(exepectedPayment);

            // Act
            var result = await _sut.Handle(command);

            // Assert
            _paymentRepositoryMock
                .Verify(
                    expression: exe => exe.AddAsync(
                        It.IsAny<Payments>()),
                    times: Times.Once);
        }
    }
}
