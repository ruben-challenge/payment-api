﻿namespace CK.Ruben.Application.Tests.Commands.Handlers
{
    using AutoFixture;
    using CK.Ruben.Application.Commands.Handlers;
    using CK.Ruben.Application.Commands.Parameters;
    using CK.Ruben.Application.Exceptions;
    using CK.Ruben.Core.Entities;
    using CK.Ruben.Core.Repositories;
    using FluentAssertions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using System;
    using System.Threading.Tasks;

    [TestClass]
    public class CancelAuthorizeCommandHandlerTests
    {
        private CancelAuthorizeCommandHandler _sut;
        private Mock<IPaymentRepository> _paymentRepositoryMock;
        private Fixture _fixture;

        [TestInitialize]
        public void TestInitialize()
        {
            _paymentRepositoryMock = new Mock<IPaymentRepository>(MockBehavior.Strict);
            _sut = new CancelAuthorizeCommandHandler(
                paymentRepository: _paymentRepositoryMock.Object);

            _fixture = new Fixture();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_WithNullMediator_ShouldThrownArgumentNullException()
        {
            new CreateAuthorizeCommandHandler(
                paymentRepository: null);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidTransactionException))]
        public async Task Handle_WithNonExistingPayment_ShouldThrownException()
        {
            // Arrange
            var command = _fixture.Create<CancelAuthorizeCommand>();

            _paymentRepositoryMock
                .Setup(exe => exe.GetByIdAsync(
                    It.IsAny<Guid>()))
                .ReturnsAsync((Payments)null);

            // Act
            await _sut.Handle(command);
        }

        [TestMethod]
        public async Task Handle_WithValidParameters_ShouldReturnMappedObject()
        {
            // Arrange
            var currentPayment = _fixture
                .Build<Payments>()
                .With(x => x.CardNumber, "4916938937029847")
                .Create();

            Payments exepectedPayment = null;

            var command = _fixture
                .Build<CancelAuthorizeCommand>()
                .With(x => x.PaymentId, currentPayment.PaymentId)
                .Create();

            _paymentRepositoryMock
                .Setup(exe => exe.GetByIdAsync(
                    It.IsAny<Guid>()))
                .ReturnsAsync(currentPayment);

            _paymentRepositoryMock
                .Setup(exe => exe.UpdateAsync(
                    It.IsAny<Payments>()))
                .Callback<Payments>(pm => exepectedPayment = pm)
                .ReturnsAsync(currentPayment);

            // Act
            var result = await _sut.Handle(command);

            // Assert
            _paymentRepositoryMock
                .Verify(
                    expression: exe => exe.GetByIdAsync(
                        It.IsAny<Guid>()),
                    times: Times.Once);

            _paymentRepositoryMock
                .Verify(
                    expression: exe => exe.UpdateAsync(
                        It.IsAny<Payments>()),
                    times: Times.Once);

            exepectedPayment
                .TransactionStatus
                .Should()
                .Be(Core.Enums.TransactionStatus.Voided);
        }
    }
}
