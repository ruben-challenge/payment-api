﻿
namespace CK.Ruben.Core.Enums
{
    public enum TransactionStatus
    {
        Created = 0,
        Voided = 1
    }
}
