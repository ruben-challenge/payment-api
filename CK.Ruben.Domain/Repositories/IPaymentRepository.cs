﻿namespace CK.Ruben.Core.Repositories
{
    using CK.Ruben.Core.Entities;
    using CK.Ruben.Core.Repositories.Base;

    public interface IPaymentRepository : IRepository<Payments>
    {
    }
}
