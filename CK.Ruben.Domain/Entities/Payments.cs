﻿namespace CK.Ruben.Core.Entities
{
    using CK.Ruben.Core.Entities.Base;
    using CK.Ruben.Core.Enums;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("payments")]
    public class Payments : Audit
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid PaymentId { get; set; }

        public string CardData { get; set; }

        public string CardNumber { get; set; }

        public string ExpiryDate { get; set; }

        public int Cvv { get; set; }

        public double Amount { get; set; }

        public double CapturedAmount { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public double AmountAvailable { get; private set; }

        public string Currency { get; set; }

        public TransactionStatus TransactionStatus { get; set; }
    }
}
