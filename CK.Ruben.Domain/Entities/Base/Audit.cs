﻿namespace CK.Ruben.Core.Entities.Base
{
    using System;
    
    public class Audit
    {
        public DateTime CreatedAt { get; set; }

        public DateTime? UpdatedAt { get; set; }
    }
}
