namespace CK.Ruben
{
    using System.Collections.Generic;
    using System.Reflection;
    using CK.Ruben.Application.Commands.Handlers;
    using CK.Ruben.Application.Commands.Parameters;
    using CK.Ruben.Application.Validators;
    using CK.Ruben.Core.Repositories;
    using CK.Ruben.Core.Repositories.Base;
    using CK.Ruben.Infrastructure.Data;
    using CK.Ruben.Infrastructure.Repositories;
    using CK.Ruben.Infrastructure.Repositories.Base;
    using FluentValidation;
    using FluentValidation.AspNetCore;
    using MediatR;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.IdentityModel.Tokens;
    using Microsoft.OpenApi.Models;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddMvc(setup =>
            {
                //...mvc setup...
            }).AddFluentValidation();

            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.Authority = "https://securetoken.google.com/ruben-57513";
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = "https://securetoken.google.com/ruben-57513",
                        ValidateAudience = true,
                        ValidAudience = "ruben-57513",
                        ValidateLifetime = true
                    };
                });

            services.AddTransient<IValidator<CreateAuthorizeCommand>, CreateAuthorizeCommandValidator>();

            services.AddDbContext<PaymentContext>(
                 m => m.UseSqlServer(Configuration.GetConnectionString("PaymentServiceDB")), ServiceLifetime.Singleton);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Payment API", Version = "v1" });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = @"JWT Authorization header using the Bearer scheme. \r\n\r\n 
                      Enter 'Bearer' [space] and then your token in the text input below.
                      \r\n\r\nExample: 'Bearer 12345abcdef'",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                  {
                    {
                      new OpenApiSecurityScheme
                      {
                        Reference = new OpenApiReference
                          {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                          },
                          Scheme = "oauth2",
                          Name = "Bearer",
                          In = ParameterLocation.Header,

                        },
                        new List<string>()
                      }
                        });
            });

            services.AddAutoMapper(typeof(Startup));
            services.AddMediatR(typeof(CreateAuthorizeCommandHandler).GetTypeInfo().Assembly);
            services.AddMediatR(typeof(CancelAuthorizeCommandHandler).GetTypeInfo().Assembly);
            services.AddMediatR(typeof(CaptureAuthorizeCommandHandler).GetTypeInfo().Assembly);
            services.AddMediatR(typeof(RefundAuthorizeCommandHandler).GetTypeInfo().Assembly);

            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient<IPaymentRepository, PaymentRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, PaymentContext paymentContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                // migrate any database changes on startup (includes initial db creation)
                paymentContext.Database.Migrate();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Payment.API v1"));

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
