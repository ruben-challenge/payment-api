﻿namespace CK.Ruben.Controllers
{
    using CK.Ruben.Application.Commands.Parameters;
    using CK.Ruben.Application.Commands.Results;
    using CK.Ruben.Application.DTO;
    using MediatR;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Threading.Tasks;

    [ApiController]
    [Route("api/refund")]
    [Authorize]
    public class RefundController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger<RefundController> _logger;

        public RefundController(
            IMediator mediator,
            ILogger<RefundController> logger)
        {
            _mediator = mediator
                ?? throw new ArgumentNullException(nameof(mediator));
            _logger = logger
                ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpPut]
        public async Task<ActionResult<RefundedAuthorizeResult>> RefundPayment([FromBody] RefundAuthorizeCommand command)
        {
            try
            {
                var result = await _mediator.Send(command);
                return Ok(result);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message, ex.StackTrace);
                return BadRequest(
                    new ErrorDto()
                    {
                        Code = System.Net.HttpStatusCode.BadRequest,
                        Message = ex.Message,
                    });
            }
        }
    }
}
