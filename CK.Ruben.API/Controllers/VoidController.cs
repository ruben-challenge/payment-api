﻿namespace CK.Ruben.Controllers
{
    using CK.Ruben.Application.Commands.Parameters;
    using CK.Ruben.Application.Commands.Results;
    using CK.Ruben.Application.DTO;
    using MediatR;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Threading.Tasks;

    [ApiController]
    [Route("api/void")]
    [Authorize]
    public class VoidController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger<VoidController> _logger;

        public VoidController(
            IMediator mediator,
            ILogger<VoidController> logger)
        {
            _mediator = mediator
                ?? throw new ArgumentNullException(nameof(mediator));
            _logger = logger
                ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpDelete]
        public async Task<ActionResult<CreateAuthorizeResult>> CancelPayment([FromBody] CancelAuthorizeCommand command)
        {
            try
            {
                var result = await _mediator.Send(command);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex.StackTrace);
                return BadRequest(
                    new ErrorDto()
                    {
                        Code = System.Net.HttpStatusCode.BadRequest,
                        Message = ex.Message,
                    });
            }
        }
    }
}
