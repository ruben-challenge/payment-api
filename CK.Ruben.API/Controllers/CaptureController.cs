﻿namespace CK.Ruben.Controllers
{
    using CK.Ruben.Application.Commands.Parameters;
    using CK.Ruben.Application.Commands.Results;
    using CK.Ruben.Application.DTO;
    using MediatR;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Threading.Tasks;

    [ApiController]
    [Route("api/capture")]
    [Authorize]
    public class CaptureController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger<CaptureController> _logger;

        public CaptureController(
            IMediator mediator,
            ILogger<CaptureController> logger)
        {
            _mediator = mediator
                ?? throw new ArgumentNullException(nameof(mediator));
            _logger = logger
                ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpPut]
        public async Task<ActionResult<CapturedAuthorizeResult>> CapturePayment([FromBody] CaptureAuthorizeCommand command)
        {
            try
            {
                var result = await _mediator.Send(command);
                return Ok(result);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message, ex.StackTrace);
                return BadRequest(
                    new ErrorDto()
                    {
                        Code = System.Net.HttpStatusCode.BadRequest,
                        Message = ex.Message,
                    });
            }
        }
    }
}
