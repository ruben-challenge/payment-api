# Payment API

## Description

In this repository you can find the API that will be the support base for the payment module.

![swagger](images/swagger.png)

To protect the api, Firebase Authentication was used so that only a user with a valid JWT can access the api's resources.

![firebase](images/firebase.png)

## Using docker-compose

To run the application using the docker-compose its possible to execute the follow command. This command will put the API and the Database running.

```bash
# start containers
$ docker-compose up -d
```

Next go to http://localhost/swagger to see the documentation of the API.

## Get Authorization Token

To obtain the Bearer token to test the api its is possible to do a POST call to firebase address bellow:

URL: https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyC9jeLkjKjHmuoBkpU1uIBzx0zC0jyQiq0

PAYLOAD:
```json
{
    "email": "merchant@ck.pt",
    "password": "P@ssw0rd",
    "returnSecureToken": true
}
```

![postman](images/postman.png)