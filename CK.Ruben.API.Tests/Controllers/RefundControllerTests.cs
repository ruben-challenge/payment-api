namespace CK.Ruben.API.Tests.Controllers
{
    using AutoFixture;
    using CK.Ruben.Application.Commands.Parameters;
    using CK.Ruben.Application.Commands.Results;
    using CK.Ruben.Controllers;
    using MediatR;
    using Microsoft.Extensions.Logging;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using System;
    using System.Threading;

    [TestClass]
    public class RefundControllerTests
    {
        private RefundController _sut;
        private Mock<IMediator> _mediatorMock;
        private Mock<ILogger<RefundController>> _loggerMock;
        private Fixture _fixture;

        [TestInitialize]
        public void TestInitialize()
        {
            _loggerMock = new Mock<ILogger<RefundController>>(MockBehavior.Strict);
            _mediatorMock = new Mock<IMediator>(MockBehavior.Strict);
            _sut = new RefundController(
                mediator: _mediatorMock.Object,
                logger: _loggerMock.Object);

            _fixture = new Fixture();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_WithNullMediator_ShouldThrownArgumentNullException()
        {
            new RefundController(
                mediator: null,
                logger: _loggerMock.Object);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_WithNullLogger_ShouldThrownArgumentNullException()
        {
            new RefundController(
                mediator: _mediatorMock.Object,
                logger: null);
        }

        [TestMethod]
        public void CreatePayment_WithCorrectParameters_ShouldCallMediator()
        {
            // Arrange
            var commandParameters = _fixture.Create<RefundAuthorizeCommand>();
            var commandResult = new RefundedAuthorizeResult()
            {
                Amount = commandParameters.Amount,
            };

            _mediatorMock
                .Setup(exe => exe.Send(
                    It.IsAny<RefundAuthorizeCommand>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(commandResult);

            // Act
            var result = _sut.RefundPayment(commandParameters);

            // Assert
            _mediatorMock
                .Verify(
                    expression: exe => exe.Send(
                        It.IsAny<RefundAuthorizeCommand>(),
                        It.IsAny<CancellationToken>()),
                    times: Times.Once);
        }
    }
}
