namespace CK.Ruben.API.Tests.Controllers
{
    using AutoFixture;
    using CK.Ruben.Application.Commands.Parameters;
    using CK.Ruben.Application.Commands.Results;
    using CK.Ruben.Controllers;
    using MediatR;
    using Microsoft.Extensions.Logging;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using System;
    using System.Threading;

    [TestClass]
    public class AuthorizeControllerTests
    {
        private AuthorizeController _sut;
        private Mock<IMediator> _mediatorMock;
        private Mock<ILogger<AuthorizeController>> _loggerMock;
        private Fixture _fixture;

        [TestInitialize]
        public void TestInitialize()
        {
            _loggerMock = new Mock<ILogger<AuthorizeController>>(MockBehavior.Strict);
            _mediatorMock = new Mock<IMediator>(MockBehavior.Strict);
            _sut = new AuthorizeController(
                mediator: _mediatorMock.Object,
                logger: _loggerMock.Object);

            _fixture = new Fixture();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_WithNullMediator_ShouldThrownArgumentNullException()
        {
            new AuthorizeController(
                mediator: null,
                logger: _loggerMock.Object);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_WithNullLogger_ShouldThrownArgumentNullException()
        {
            new AuthorizeController(
                mediator: _mediatorMock.Object,
                logger: null);
        }

        [TestMethod]
        public void CreatePayment_WithCorrectParameters_ShouldCallMediator()
        {
            // Arrange
            var commandParameters = _fixture.Create<CreateAuthorizeCommand>();
            var commandResult = new CreateAuthorizeResult()
            {
                PaymentId = Guid.NewGuid(),
                Amount = commandParameters.Amount,
                Currency = commandParameters.Currency
            };

            var expectedResult = new CreateAuthorizeResult()
            {
                Amount = commandParameters.Amount,
                Currency = commandParameters.Currency
            };

            _mediatorMock
                .Setup(exe => exe.Send(
                    It.IsAny<CreateAuthorizeCommand>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(commandResult);


            // Act
            var result = _sut.CreatePayment(commandParameters);

            // Assert
            _mediatorMock
                .Verify(
                    expression: exe => exe.Send(
                        It.IsAny<CreateAuthorizeCommand>(),
                        It.IsAny<CancellationToken>()),
                    times: Times.Once);
        }
    }
}
