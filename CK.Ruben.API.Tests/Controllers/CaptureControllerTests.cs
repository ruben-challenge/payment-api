namespace CK.Ruben.API.Tests.Controllers
{
    using AutoFixture;
    using CK.Ruben.Application.Commands.Parameters;
    using CK.Ruben.Application.Commands.Results;
    using CK.Ruben.Controllers;
    using MediatR;
    using Microsoft.Extensions.Logging;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using System;
    using System.Threading;

    [TestClass]
    public class CaptureControllerTests
    {
        private CaptureController _sut;
        private Mock<IMediator> _mediatorMock;
        private Mock<ILogger<CaptureController>> _loggerMock;
        private Fixture _fixture;

        [TestInitialize]
        public void TestInitialize()
        {
            _loggerMock = new Mock<ILogger<CaptureController>>(MockBehavior.Strict);
            _mediatorMock = new Mock<IMediator>(MockBehavior.Strict);
            _sut = new CaptureController(
                mediator: _mediatorMock.Object,
                logger: _loggerMock.Object);

            _fixture = new Fixture();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_WithNullMediator_ShouldThrownArgumentNullException()
        {
            new CaptureController(
                mediator: null,
                logger: _loggerMock.Object);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_WithNullLogger_ShouldThrownArgumentNullException()
        {
            new CaptureController(
                mediator: _mediatorMock.Object,
                logger: null);
        }

        [TestMethod]
        public void CapturePayment_WithCorrectParameters_ShouldCallMediator()
        {
            // Arrange
            var commandParameters = _fixture.Create<CaptureAuthorizeCommand>();
            var commandResult = _fixture.Create<CapturedAuthorizeResult>();

            _mediatorMock
                .Setup(exe => exe.Send(
                    It.IsAny<CaptureAuthorizeCommand>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(commandResult);


            // Act
            var result = _sut.CapturePayment(commandParameters);

            // Assert
            _mediatorMock
                .Verify(
                    expression: exe => exe.Send(
                        It.IsAny<CaptureAuthorizeCommand>(),
                        It.IsAny<CancellationToken>()),
                    times: Times.Once);
        }
    }
}
