﻿namespace CK.Ruben.Infrastructure.Repositories.Base
{
    using CK.Ruben.Core.Repositories.Base;
    using CK.Ruben.Infrastructure.Data;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class Repository<T> : IRepository<T> where T : class
    {
        protected readonly PaymentContext _paymentContext;

        public Repository(PaymentContext paymentContext)
        {
            _paymentContext = paymentContext;
        }
        public async Task<T> AddAsync(T entity)
        {
            if (entity is null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            await _paymentContext.Set<T>().AddAsync(entity);
            await _paymentContext.SaveChangesAsync();
            return entity;
        }

        public async Task<T> DeleteAsync(T entity)
        {
            if (entity is null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            _paymentContext.Set<T>().Remove(entity);
            await _paymentContext.SaveChangesAsync();

            return entity;
        }

        public async Task<IReadOnlyList<T>> GetAllAsync()
        {
            return await _paymentContext.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _paymentContext.Set<T>().FindAsync(id);
        }

        public async Task<T> UpdateAsync(T entity)
        {
            if(entity is null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            _paymentContext.Set<T>().Update(entity);
            await _paymentContext.SaveChangesAsync();

            return entity;
        }
    }
}
