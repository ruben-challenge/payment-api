﻿namespace CK.Ruben.Infrastructure.Repositories
{
    using CK.Ruben.Core.Entities;
    using CK.Ruben.Core.Repositories;
    using CK.Ruben.Infrastructure.Data;
    using CK.Ruben.Infrastructure.Repositories.Base;

    public class PaymentRepository : Repository<Payments>, IPaymentRepository
    {
        public PaymentRepository(PaymentContext ckContext) : base(ckContext)
        {
        }
    }
}
