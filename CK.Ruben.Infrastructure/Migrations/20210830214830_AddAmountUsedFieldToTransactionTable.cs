﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CK.Ruben.Infrastructure.Migrations
{
    public partial class AddAmountUsedFieldToTransactionTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<float>(
                name: "AmountUsed",
                table: "payments",
                nullable: false,
                defaultValue: 0f);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AmountUsed",
                table: "payments");
        }
    }
}
