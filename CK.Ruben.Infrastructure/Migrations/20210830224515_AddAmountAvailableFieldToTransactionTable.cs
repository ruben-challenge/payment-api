﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CK.Ruben.Infrastructure.Migrations
{
    public partial class AddAmountAvailableFieldToTransactionTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AmountUsed",
                table: "payments");

            migrationBuilder.AlterColumn<double>(
                name: "Amount",
                table: "payments",
                nullable: false,
                oldClrType: typeof(float),
                oldType: "real");

            migrationBuilder.AddColumn<double>(
                name: "CapturedAmount",
                table: "payments",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "AmountAvailable",
                table: "payments",
                nullable: false,
                computedColumnSql: "[Amount] - [CapturedAmount]");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AmountAvailable",
                table: "payments");

            migrationBuilder.DropColumn(
                name: "CapturedAmount",
                table: "payments");

            migrationBuilder.AlterColumn<float>(
                name: "Amount",
                table: "payments",
                type: "real",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AddColumn<float>(
                name: "AmountUsed",
                table: "payments",
                type: "real",
                nullable: false,
                defaultValue: 0f);
        }
    }
}
