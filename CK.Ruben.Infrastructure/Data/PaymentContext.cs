﻿namespace CK.Ruben.Infrastructure.Data
{
    using CK.Ruben.Core.Entities;
    using Microsoft.EntityFrameworkCore;

    public class PaymentContext : DbContext
    {
        public PaymentContext(DbContextOptions<PaymentContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Payments>()
                .Property(u => u.AmountAvailable)
                .HasComputedColumnSql("[Amount] - [CapturedAmount]");
        }

        public DbSet<CK.Ruben.Core.Entities.Payments> Payments { get; set; }
    }
}
