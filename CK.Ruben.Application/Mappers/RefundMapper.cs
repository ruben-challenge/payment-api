﻿namespace CK.Ruben.Application.Mappers
{
    using CK.Ruben.Application.Commands.Results;
    using CK.Ruben.Core.Entities;

    public static class RefundMapper
    {
        public static RefundedAuthorizeResult ToRefundedResult(this Payments from)
        {
            if (from is null)
            {
                return default;
            }

            return new RefundedAuthorizeResult()
            {
                Amount = from.Amount - from.CapturedAmount,
                Currency = from.Currency
            };
        }
    }
}
