﻿namespace CK.Ruben.Application.Mappers
{
    using CK.Ruben.Application.Commands.Results;
    using CK.Ruben.Core.Entities;

    public static class VoidMapper
    {
        public static CancelAuthorizeResult ToCancelResult(this Payments from)
        {
            if (from is null)
            {
                return default;
            }

            return new CancelAuthorizeResult()
            {
                Amount = from.Amount,
                Currency = from.Currency
            };
        }
    }
}
