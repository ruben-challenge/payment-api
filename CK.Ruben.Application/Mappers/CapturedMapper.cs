﻿namespace CK.Ruben.Application.Mappers
{
    using CK.Ruben.Application.Commands.Results;
    using CK.Ruben.Core.Entities;

    public static class CapturedMapper
    {
        public static CapturedAuthorizeResult ToCapturedResult(this Payments from)
        {
            if (from is null)
            {
                return default;
            }

            return new CapturedAuthorizeResult()
            {
                Amount = from.Amount - from.CapturedAmount,
                Currency = from.Currency
            };
        }
    }
}
