﻿namespace CK.Ruben.Application.Mappers
{
    using CK.Ruben.Application.Commands.Parameters;
    using CK.Ruben.Application.Commands.Results;
    using CK.Ruben.Core.Entities;

    public static class AuthorizeMapper
    {
        public static CreateAuthorizeResult ToResult(this Payments from)
        {
            if (from is null)
            {
                return default;
            }

            return new CreateAuthorizeResult()
            {
                PaymentId = from.PaymentId,
                Amount = from.Amount,
                Currency = from.Currency
            };
        }

        public static Payments ToEntity(this CreateAuthorizeCommand from)
        {
            if(from is null)
            {
                return default;
            }

            return new Payments()
            {
                CardData = from.CardData,

                CardNumber = from.CardNumber,

                ExpiryDate = from.ExpiryDate,

                Cvv = from.Cvv,

                Amount = from.Amount,

                Currency = from.Currency,
            };
        }
    }
}
