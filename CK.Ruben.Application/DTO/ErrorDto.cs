﻿namespace CK.Ruben.Application.DTO
{
    using System.Net;

    public class ErrorDto
    {
        public HttpStatusCode Code { get; set; }

        public string Message { get; set; }
    }
}
