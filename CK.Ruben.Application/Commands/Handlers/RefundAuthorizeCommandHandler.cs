﻿namespace CK.Ruben.Application.Commands.Handlers
{
    using CK.Ruben.Application.Commands.Parameters;
    using CK.Ruben.Application.Commands.Results;
    using CK.Ruben.Application.Exceptions;
    using CK.Ruben.Application.Mappers;
    using CK.Ruben.Core.Repositories;
    using MediatR;
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public class RefundAuthorizeCommandHandler : IRequestHandler<RefundAuthorizeCommand, RefundedAuthorizeResult>
    {
        private readonly IPaymentRepository _paymentRepository;

        public RefundAuthorizeCommandHandler(IPaymentRepository paymentRepository)
        {
            _paymentRepository = paymentRepository
                ?? throw new ArgumentNullException(nameof(paymentRepository));
        }

        public async Task<RefundedAuthorizeResult> Handle(RefundAuthorizeCommand request, CancellationToken cancellationToken = default)
        {
            var currentPayment = await this._paymentRepository.GetByIdAsync(request.PaymentId);

            if (currentPayment == null)
            {
                throw new InvalidTransactionException(request.PaymentId);
            }

            if(currentPayment.TransactionStatus == Core.Enums.TransactionStatus.Voided)
            {
                throw new VoidedTransactionException(request.PaymentId);
            }

            if (request.Amount > currentPayment.CapturedAmount)
            {
                throw new InvalidRefundAmountException(currentPayment.AmountAvailable);
            }

            // subtract the refunded amount to the Captured amount
            currentPayment.CapturedAmount -= request.Amount;
            currentPayment.UpdatedAt = DateTime.UtcNow;

            await _paymentRepository.UpdateAsync(currentPayment);

            return currentPayment.ToRefundedResult();
        }

    }
}
