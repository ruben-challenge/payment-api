﻿namespace CK.Ruben.Application.Commands.Handlers
{
    using CK.Ruben.Application.Commands.Parameters;
    using CK.Ruben.Application.Commands.Results;
    using CK.Ruben.Application.Exceptions;
    using CK.Ruben.Application.Mappers;
    using CK.Ruben.Application.Validators;
    using CK.Ruben.Core.Repositories;
    using MediatR;
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public class CreateAuthorizeCommandHandler : IRequestHandler<CreateAuthorizeCommand, CreateAuthorizeResult>
    {
        private readonly IPaymentRepository _paymentRepository;

        public CreateAuthorizeCommandHandler(IPaymentRepository paymentRepository)
        {
            _paymentRepository = paymentRepository
                ?? throw new ArgumentNullException(nameof(paymentRepository));
        }

        public async Task<CreateAuthorizeResult> Handle(CreateAuthorizeCommand request, CancellationToken cancellationToken = default)
        {
            var entity = request.ToEntity();

            if (entity is null)
            {
                throw new ApplicationException("Issue with mapper");
            }

            if (!request.CardNumber.LuhnCheck())
            {
                throw new InvalidCreditCardNumbertException();
            }

            entity.PaymentId = Guid.NewGuid();
            entity.CreatedAt = DateTime.UtcNow;
            entity.TransactionStatus = Core.Enums.TransactionStatus.Created;

            var newPayment = await _paymentRepository.AddAsync(entity);

            return newPayment.ToResult();
        }

    }
}
