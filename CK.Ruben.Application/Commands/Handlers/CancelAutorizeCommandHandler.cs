﻿namespace CK.Ruben.Application.Commands.Handlers
{
    using CK.Ruben.Application.Commands.Parameters;
    using CK.Ruben.Application.Commands.Results;
    using CK.Ruben.Application.Exceptions;
    using CK.Ruben.Application.Mappers;
    using CK.Ruben.Core.Repositories;
    using MediatR;
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public class CancelAuthorizeCommandHandler : IRequestHandler<CancelAuthorizeCommand, CancelAuthorizeResult>
    {
        private readonly IPaymentRepository _paymentRepository;

        public CancelAuthorizeCommandHandler(IPaymentRepository paymentRepository)
        {
            _paymentRepository = paymentRepository 
                ?? throw new ArgumentNullException(nameof(paymentRepository));
        }

        public async Task<CancelAuthorizeResult> Handle(CancelAuthorizeCommand request, CancellationToken cancellationToken = default)
        {
            var currentPayment = await this._paymentRepository.GetByIdAsync(request.PaymentId);

            if(currentPayment == null)
            {
                throw new InvalidTransactionException(request.PaymentId);
            }

            // Change the payment status
            currentPayment.TransactionStatus = Core.Enums.TransactionStatus.Voided;
            currentPayment.UpdatedAt = DateTime.UtcNow;

            await _paymentRepository.UpdateAsync(currentPayment);

            return currentPayment.ToCancelResult();
        }

    }
}
