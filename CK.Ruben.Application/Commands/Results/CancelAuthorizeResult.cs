﻿namespace CK.Ruben.Application.Commands.Results
{
    public class CancelAuthorizeResult
    {
        public double Amount { get; set; }

        public string Currency { get; set; }
    }
}
