﻿namespace CK.Ruben.Application.Commands.Results
{
    using System;

    public class CreateAuthorizeResult
    {
        public Guid PaymentId { get; set; }

        public double Amount { get; set; }

        public string Currency { get; set; }
    }
}
