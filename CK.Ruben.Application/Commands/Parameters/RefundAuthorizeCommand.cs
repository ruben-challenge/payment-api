﻿namespace CK.Ruben.Application.Commands.Parameters
{
    using CK.Ruben.Application.Commands.Results;
    using MediatR;
    using System;

    public class RefundAuthorizeCommand : IRequest<RefundedAuthorizeResult>
    {
        public Guid PaymentId { get; set; }
        
        public float Amount { get; set; }
    }
}
