﻿namespace CK.Ruben.Application.Commands.Parameters
{
    using CK.Ruben.Application.Commands.Results;
    using MediatR;
    using System;

    public class CancelAuthorizeCommand : IRequest<CancelAuthorizeResult>
    {
        public Guid PaymentId { get; set; }
    }
}
