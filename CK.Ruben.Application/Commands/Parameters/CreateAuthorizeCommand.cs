﻿namespace CK.Ruben.Application.Commands.Parameters
{
    using CK.Ruben.Application.Commands.Results;
    using MediatR;
    using System.ComponentModel.DataAnnotations;

    public class CreateAuthorizeCommand : IRequest<CreateAuthorizeResult>
    {
        public string CardData { get; set; }

        public string CardNumber { get; set; }

        public string ExpiryDate { get; set; }

        public int Cvv { get; set; }

        public float Amount { get; set; }

        public string Currency { get; set; }
    }
}
