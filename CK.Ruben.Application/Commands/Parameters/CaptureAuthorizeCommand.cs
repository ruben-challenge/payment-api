﻿namespace CK.Ruben.Application.Commands.Parameters
{
    using CK.Ruben.Application.Commands.Results;
    using MediatR;
    using System;

    public class CaptureAuthorizeCommand : IRequest<CapturedAuthorizeResult>
    {
        public Guid PaymentId { get; set; }
        
        public float Amount { get; set; }
    }
}
