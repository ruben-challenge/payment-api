﻿namespace CK.Ruben.Application.Validators
{
    using CK.Ruben.Application.Commands.Parameters;
    using FluentValidation;

    public class CreateAuthorizeCommandValidator : AbstractValidator<CreateAuthorizeCommand>
    {
        public CreateAuthorizeCommandValidator()
        {
            RuleFor(x => x.CardData).NotNull();
            RuleFor(x => x.CardNumber).NotNull();
            RuleFor(x => x.ExpiryDate).NotNull();
            RuleFor(x => x.Amount).GreaterThan(0);
            RuleFor(x => x.CardNumber).CreditCard();
        }
    }
}
