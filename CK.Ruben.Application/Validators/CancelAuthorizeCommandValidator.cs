﻿namespace CK.Ruben.Application.Validators
{
    using CK.Ruben.Application.Commands.Parameters;
    using FluentValidation;

    public class CancelAuthorizeCommandValidator : AbstractValidator<CancelAuthorizeCommand>
    {
        public CancelAuthorizeCommandValidator()
        {
            RuleFor(x => x.PaymentId).NotNull();
        }
    }
}
