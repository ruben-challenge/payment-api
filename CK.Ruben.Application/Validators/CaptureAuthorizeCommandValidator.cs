﻿namespace CK.Ruben.Application.Validators
{
    using CK.Ruben.Application.Commands.Parameters;
    using FluentValidation;

    public class CaptureAuthorizeCommandValidator : AbstractValidator<CaptureAuthorizeCommand>
    {
        public CaptureAuthorizeCommandValidator()
        {
            RuleFor(x => x.PaymentId).NotNull();
            RuleFor(x => x.Amount).GreaterThan(0);
        }
    }
}
