﻿namespace CK.Ruben.Application.Exceptions
{
    using System;

    public class InvalidRefundAmountException : Exception
    {
        private const string InvalidMessage = "The current refunded ammount is not valid, the remain value is {0}";

        public InvalidRefundAmountException(double availableAmount)
            : base(string.Format(InvalidMessage, availableAmount.ToString()))
        {
        }
    }
}
