﻿namespace CK.Ruben.Application.Exceptions
{
    using System;

    public class InvalidTransactionException : Exception
    {
        private const string InvalidMessage = "The current transaction {0} does't exist in the system";

        public InvalidTransactionException(Guid paymentId)
            : base(string.Format(InvalidMessage, paymentId.ToString()))
        {
        }
    }
}
