﻿namespace CK.Ruben.Application.Exceptions
{
    using System;

    public class InvalidCreditCardNumbertException : Exception
    {
        private const string InvalidMessage = "The card number is not correct";

        public InvalidCreditCardNumbertException()
            : base(InvalidMessage)
        {
        }
    }
}
