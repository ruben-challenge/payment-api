﻿namespace CK.Ruben.Application.Exceptions
{
    using System;

    public class InvalidRefundException : Exception
    {
        private const string InvalidMessage = "The current refunded ammount is greater than the captured amount, the captured amount is {0}";

        public InvalidRefundException(double availableAmount)
            : base(string.Format(InvalidMessage, availableAmount.ToString()))
        {
        }
    }
}
