﻿namespace CK.Ruben.Application.Exceptions
{
    using System;

    public class VoidedTransactionException : Exception
    {
        private const string InvalidMessage = "The current transaction {0} is voided";

        public VoidedTransactionException(Guid paymentId)
            : base(string.Format(InvalidMessage, paymentId.ToString()))
        {
        }
    }
}
