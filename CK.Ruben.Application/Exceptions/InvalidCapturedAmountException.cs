﻿namespace CK.Ruben.Application.Exceptions
{
    using System;

    public class InvalidCapturedException : Exception
    {
        private const string InvalidMessage = "The current captured ammount is not valid, the remain value is {0}";

        public InvalidCapturedException(double availableAmount)
            : base(string.Format(InvalidMessage, availableAmount.ToString()))
        {
        }
    }
}
